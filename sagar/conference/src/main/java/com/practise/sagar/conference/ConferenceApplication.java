package com.practise.sagar.conference;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ConferenceApplication {

	public static void main(String[] args) {
		//This was original code commented
		//SpringApplication.run(ConferenceApplication.class, args);
		
		//created object using ApplicationContext to access dependency injection
		ApplicationContext context = SpringApplication.run(ConferenceApplication.class, args);
		
		//getBean used to get object of the class 		
		DependencyInjectionPractise dip = context.getBean(DependencyInjectionPractise.class);
		dip.code();
	}

}
