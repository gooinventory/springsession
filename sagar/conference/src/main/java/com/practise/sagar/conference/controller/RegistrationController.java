package com.practise.sagar.conference.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.practise.sagar.conference.model.Registration;

@Controller
public class RegistrationController {
	
@GetMapping("registration")
public String getregistration(@ModelAttribute("registration") Registration registration){
	  
	return "registration";
	
}

@PostMapping("registration")
public String addregistration(@Valid @ModelAttribute("registration") Registration registration,BindingResult result){
	 if(result.hasErrors()) {
		// System.out.println("error::: name must not be empty");
		 return "registration";
		 
	 }
	  System.out.println("RegistrationName:-"+registration.getName());
	return "redirect:index.html";
		
}


}
