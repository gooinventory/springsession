package com.practise.sagar.conference.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class GreetingController {
	
	@GetMapping("greeting")
	public String greeting (Map<String, Object> model) {
		model.put("message", "Hello Sagar");
		return "greeting";
		
	}

}
