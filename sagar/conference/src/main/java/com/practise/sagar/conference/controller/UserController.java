package com.practise.sagar.conference.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.practise.sagar.conference.model.User;

@RestController
public class UserController {
 
	@GetMapping("/user")
	public User getUser(@RequestParam(value = "firstname", defaultValue = "sagar") String firstname,
			            @RequestParam(value = "lastname", defaultValue = "kumar") String lastname,
			            @RequestParam(value = "age", defaultValue = "28") int age) 	
	{
		
		User user = new User();
		
		user.setFirstname(firstname);
		user.setLastname(lastname);
		user.setAge(age);
		
		return user;
		
	}
	
	@PostMapping("/user")
	public User postUser(User user) {
		
		System.out.println("PostmanPosttest:="+user.getFirstname()+","+user.getLastname()+","+user.getAge());
		
		return user;
		
	}
	
	
}
