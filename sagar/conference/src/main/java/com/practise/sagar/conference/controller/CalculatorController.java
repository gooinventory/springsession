package com.practise.sagar.conference.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.practise.sagar.conference.model.Registration;

@Controller
public class CalculatorController {

	@GetMapping("calculator")
	public String getregistration(){
		 System.out.println("Entered Calculator screen");
		return "calculator";
		
	}
	
	@RequestMapping("add")
	public ModelAndView Adding(@RequestParam("num1") int num1,@RequestParam("num2") int num2) {
		
		ModelAndView mv = new ModelAndView("result");
		//mv.setViewName("result");
		int num3 = num1+num2;
		mv.addObject("num3", num3);
		return mv;
	}

	
}
