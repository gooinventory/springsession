package com.practise.sagar.conference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class DependencyInjectionPractise {
	
	//Autowiring the class SpringBootAutowirePractise to fetch data without creating object
	@Autowired
	SpringBootAutowirePractise sap;
	
	
	public void code() {
		
		System.out.println("Dependency Injection Working");
		
		//fetching data from SpringBootAutowirePractise class
		sap.Autowirepractise();
	}

}
