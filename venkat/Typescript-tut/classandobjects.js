"use strict";
exports.__esModule = true;
var MyCode = /** @class */ (function () {
    function MyCode(name, use) {
        this._name = name;
        this.use = use;
    }
    MyCode.prototype.getName = function () {
        return this._name;
    };
    MyCode.prototype.setName = function (name) {
        this._name = name;
    };
    return MyCode;
}());
exports.MyCode = MyCode;
