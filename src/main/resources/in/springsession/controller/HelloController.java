package in.springsession.controller;


@Controller
public class HelloController {

	@RequestMapping(value ="/greeting")
	public String sayHello (Model model) {
		
		model.addAttribute("greeting", "Hello Team");
		
		return "hello";
	}
	
}
